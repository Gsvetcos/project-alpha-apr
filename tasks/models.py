from django.db import models
from projects.models import Project
from django.conf import settings

# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200)
    is_completed = models.BooleanField(default=False)
    start_date = models.DateTimeField(null=False)
    due_date = models.DateTimeField(null=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )
